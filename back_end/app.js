
var mongocnt = require("mongodb");
var config = require("./config");
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var jwt = require("jsonwebtoken");
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var bcrypt = require('bcrypt-nodejs');
var multer = require('multer');
var path = require('path');
var express = require('express');
var app = express();
var server = require('http').Server(app);

var listener =server.listen(config.port, function () {
  console.log('Listening on port ' + listener.address().port); //Listening on port 8888
})



var url = config.db_url;
mongoose.Promise = global.Promise
mongoose.connect(url, function (err, result) {
  if (err) {
    console.log("err####", err);
  }
});


require("./router/auth.js");
app.use(express.static("uploads"));
app.use(express.static("../front_end"));



app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));


app.use(morgan('dev'));
app.use(session({ secret: '1234567890QWERTY' }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(morgan("dev"));
app.use(flash());
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
  next();
});

var client = require("./controller/client");
var employee = require("./controller/employee");
var test = require("./controller/test");

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/index.html");
});


app.post('/login', passport.authenticate('local-login', {
  successRedirect: '/success',
  failureRedirect: '/login',
  failureFlash: true
}));

app.get('/login', function (req, res) {
  res.send({ message: req.flash('authMessage') });
});

app.get('/success', function (req, res) {
  res.send({ 'token': req.session.token, 'currentuser': req.session.username });
});


var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads/logo');
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});

var storageemp = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads/employee');
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});

var uemppload = multer({ storage: storageemp });
var userupload = multer({ storage: storage });

app.post('/addcompany', userupload.single('file'), client.save);
app.get('/clientlist', client.clientlist);
app.post('/deletedata', client.deletedata);
app.post('/getclientdetails', client.getclientdetails);


app.post('/addemp', uemppload.single('file'), client.addemp);
app.post('/emplist', client.emplist);
app.post('/deleteempdata', client.deleteempdata);

app.post('/getempdetails', employee.getempdetails);
app.post('/addtest', employee.addtest);
app.post('/gettestlist', employee.gettestlist);
app.post('/deletetest', employee.deletetest);

app.post('/gettestdetails', test.gettestdetails);


