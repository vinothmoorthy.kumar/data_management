var config = {};

config.port = process.env.WEB_PORT || 8080;
config.db_url = 'mongodb://localhost:27017/client_data';

module.exports = config;