var express = require('express');
var router = express.Router();
var passport = require('passport');
var app=express();
var LocalStrategy   = require('passport-local').Strategy;
var Schema = require('../controller/mongodbconnection.js');
var bcrypt   = require('bcrypt-nodejs');
var jwt        = require("jsonwebtoken");
var flash = require('connect-flash');

app.use(passport.initialize());

	
	passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });



  
	
	passport.use('local-login', new LocalStrategy({
        emailField : 'emailid',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req, email,password, done) {

        process.nextTick(function() {



			Schema.users.findOne({ 'username' :  req.body.username }, function(err, user) {
				if (err)
					return done(err);
				if (!user) {
					return done(null, false, req.flash('authMessage', "User Name does't exist"));
				} 				

				if (user.status==0) {
					return done(null, false, req.flash('authMessage', "Your account has been deleted"));
				} 	

				if (!validPassword(user,password)){
					return done(null, false, req.flash('authMessage', "Wrong Password"));
				}

				req.session.token=jwt.sign(user, "hivinoth");
				req.session.username=user.username;
				return done(null, user);
			});    

        });

    }));
	
	var generateHash = function(password) {
		return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
	};

	var validPassword = function(user,password) {
		
		return bcrypt.compareSync(password, user.password);
	};

