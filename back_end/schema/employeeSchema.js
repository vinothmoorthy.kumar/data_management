 var mongoose= require('mongoose'); 
 var Schema = mongoose.Schema;
 var employeeSchema=new mongoose.Schema({
		name:String,
		address:String,
        email:String,
        phone_number:String,
        status:{type:Number, default:1},
        company:{ type: Schema.Types.ObjectId, ref: 'employees' },
        avatar:String
},{timestamps:true}); 
module.exports=
{
	employeeSchema:employeeSchema
}