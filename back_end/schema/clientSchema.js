 var mongoose= require('mongoose'); 
 
 var clientSchema=new mongoose.Schema({
		name:String,
		address:String,
        email:String,
        phone_number:String,
        status:{type:Number, default:1},
        siteurl:String,
        logo:String
},{timestamps:true}); 
module.exports=
{
	clientSchema:clientSchema
}
