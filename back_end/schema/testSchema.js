 var mongoose= require('mongoose'); 
 var Schema = mongoose.Schema;
 var testSchema=new mongoose.Schema({
		title:String, 
        result:{type:Boolean, default:false},
        comment:String,
        status:{type:Number, default:1},
        employee:{ type: Schema.Types.ObjectId, ref: 'employees' },
        company:{ type: Schema.Types.ObjectId, ref: 'company' },
},{timestamps:true}); 
module.exports=
{
	testSchema:testSchema
}