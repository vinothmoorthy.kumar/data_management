var express = require('express');
var Schema = require('./mongodbconnection.js');
var mongoose = require('mongoose');

exports.save = function (req, res) {
    var data = req.body;
    if (req.file) {
        data.logo = "logo/" + req.file.originalname;
    }
    var datasave = new Schema.company(data);

    if (req.body._id) {
        Schema.company.findOneAndUpdate({ _id: req.body._id }, data, function (err, result) {
            if (err) throw err;
            res.send(result);
        });
    } else {
        datasave.save(function (err, result) {
            res.send(result);
        });
    }
};

exports.clientlist = function (req, res) {
    Schema.company.find({ status: 1 }, function (err, data) {
        res.send(data);
    });

};



exports.deletedata = function (req, res) {

    Schema.company.remove({ _id: req.body._id }, function (err, data) {
        if (err) throw err;

        Schema.employee.remove({ company: mongoose.Types.ObjectId(req.body._id) }, function (err, data) {
            if (err) throw err;
            res.send(data);
        });
    });

};




exports.getclientdetails = function (req, res) {
    Schema.company.find({ _id: mongoose.Types.ObjectId(req.body._id) }, function (err, data) {
        res.send(data);
    });

};



exports.deleteempdata = function (req, res) {
    Schema.employee.remove({ _id: req.body._id }, function (err, data) {
        if (err) throw err;
        res.send(data);
    });
};

exports.addemp = function (req, res) {
    var data = req.body;
    if (req.file) {
        data.avatar = "employee/" + req.file.originalname;
    }
    var datasave = new Schema.employee(data);

    if (req.body._id) {
        Schema.employee.findOneAndUpdate({ _id: req.body._id }, data, function (err, result) {
            if (err) throw err;
            res.send(result);
        });
    } else {
        datasave.save(function (err, result) {
            res.send(result);
        });
    }

};


exports.emplist = function (req, res) {

    Schema.employee.find({ company: mongoose.Types.ObjectId(req.body.id) }, function (err, data) {
        res.send(data);
    });
};








