
var express = require('express');
var Schema = require('./mongodbconnection.js');
var mongoose = require('mongoose');

exports.getempdetails = function (req, res) {
    Schema.employee.find({ _id: mongoose.Types.ObjectId(req.body._id) }, function (err, data) {
        res.send(data);
    });
};


exports.gettestlist = function (req, res) {
    Schema.tests.find({ employee: mongoose.Types.ObjectId(req.body._id) }, function (err, data) {
        res.send(data);
    });
};


exports.addtest = function (req, res) {
    
    var data = req.body;
    var datasave = new Schema.tests(data);

    if (req.body._id) {
        Schema.tests.findOneAndUpdate({ _id: req.body._id }, data, function (err, result) {
            if (err) throw err;
            res.send(result);
        });
    } else {
        datasave.save(function (err, result) {
            res.send(result);
        });
    }
};


exports.deletetest = function (req, res) {
    Schema.tests.remove({ _id: req.body._id }, function (err, data) {
        if (err) throw err;
        res.send(data);
    });
};





