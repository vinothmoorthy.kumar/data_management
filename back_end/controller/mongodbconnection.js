var mongoose = require('mongoose');
var User_Schema = require('../schema/userSchema');
var client_Schema = require('../schema/clientSchema');
var employee_Schema = require('../schema/employeeSchema');
var test_Schema = require('../schema/testSchema');


var users = mongoose.model("users", User_Schema.userSchema, "users");
var company = mongoose.model("company", client_Schema.clientSchema, "company");
var employee = mongoose.model("employees", employee_Schema.employeeSchema, "employees");
var tests = mongoose.model("tests", test_Schema.testSchema, "tests");


module.exports =
    {
        users: users,
        company:company,
        employee:employee,
        tests:tests
    }