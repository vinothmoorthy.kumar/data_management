angular.module('datamanage').factory("clientfactory", function ($http, Upload, $q) {
    var factory = {};


    factory.addemp = function (data) {
        return Upload.upload({
            url: '/addemp',
            data: data
        }).success(function (response) {
        }).error();
    };

    factory.addcompany = function (data) {
        return Upload.upload({
            url: '/addcompany',
            data: data
        }).success(function (response) {
        }).error();
    };



    factory.getemplist = function (data) {
        var deferred = $q.defer();
        $http({
            method: 'post',
            url: '/emplist',
            data: data
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };

    factory.getlist = function (data) {

        var deferred = $q.defer();
        $http({
            method: 'get',
            url: '/clientlist',
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };


    

    factory.deleteempdata = function (data) {

        var deferred = $q.defer();
        $http({
            method: 'post',
            url: '/deleteempdata',
            data: data
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };

    factory.deletedata = function (data) {

        var deferred = $q.defer();
        $http({
            method: 'post',
            url: '/deletedata',
            data: data
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };

    factory.getclientdetails = function (data) {

        var deferred = $q.defer();
        $http({
            method: 'post',
            url: '/getclientdetails',
            data: data
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };




    return factory;

});