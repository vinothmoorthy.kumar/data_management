'use strict';

angular.module('datamanage')
    .controller('clientViewCtl', ["$scope", '$mdSidenav', '$mdToast', 'clientfactory', 'clientviewresolve', '$state', '$mdDialog', '$stateParams',
        function ($scope, $mdSidenav, $mdToast, clientfactory, clientviewresolve, $state, $mdDialog, $stateParams) {
            var clvew = this;
            clvew.project = clientviewresolve[0];

            clvew.save = function (formdata) {
                var data = formdata;
                if (data) {
                    if ($stateParams.id) {
                        data._id = $stateParams.id;
                    }
                    if (data.name && data.address && data.email && data.phone_number && data.siteurl) {
                        clientfactory.addcompany(data).then(function (response) {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Submited Successfully')
                                    .hideDelay(3000)
                            );
                        })
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Some Fields are missing')
                                .hideDelay(3000)
                        );
                    }
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Fill All The Fields')
                            .hideDelay(3000)
                    );
                }

            }


            clvew.getclientdata = function () {
                clientfactory.getemplist({ id: $stateParams.id }).then(function (response) {
                    clvew.empdata = response;
                });
            }

            clvew.deletecompany = function (data) {
                clientfactory.deleteempdata(data).then(function (response) {
                    clientfactory.getemplist({ id: $stateParams.id }).then(function (response) {
                        clvew.empdata = response;
                    });
                });
            }

            clvew.goToemp = function (person, event) {
                $state.go('main.empview', { id: person._id });
            }
            clvew.saveemp = function (formdata) {
                var data = formdata;
                data.company = $stateParams.id;
                if (data) {
                    if (data.name && data.address && data.email && data.phone_number) {
                        clientfactory.addemp(data).then(function (response) {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Submited Successfully')
                                    .hideDelay(3000)
                            );
                        })
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Some Fields are missing')
                                .hideDelay(3000)
                        );
                    }
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Fill All The Fields')
                            .hideDelay(3000)
                    );
                }

            }
        }]);