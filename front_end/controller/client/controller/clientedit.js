'use strict';

angular.module('datamanage')
    .controller('clientCtrl', ["$scope", '$mdSidenav', '$mdToast', 'clientfactory', 'clientlistresolve', '$state', '$mdDialog', '$stateParams',
        function ($scope, $mdSidenav, $mdToast, clientfactory, clientlistresolve, $state, $mdDialog, $stateParams) {
            $scope.people = [
                { name: "vinoth" }, { name: "priya" }
            ];

            var cledctl = this;

            cledctl.deletecompany = function (data) {
                clientfactory.deletedata(data).then(function (response) {

                    clientfactory.getlist().then(function (response) {
                        cledctl.clientdata = response;
                    });
                });
            }

            


            cledctl.clientdata = clientlistresolve;

            cledctl.getclientdata = function () {
                clientfactory.getlist().then(function (response) {
                    cledctl.clientdata  = response;
                });
            }

            cledctl.save = function (formdata) {
                var data = formdata;
                if (data) {
                    if (data.name && data.address && data.email && data.phone_number && data.siteurl) {
                        clientfactory.addcompany(data).then(function (response) {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Submited Successfully')
                                    .hideDelay(3000)
                            );
                            cledctl.project = {};
                        })
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Some Fields are missing')
                                .hideDelay(3000)
                        );
                    }
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Fill All The Fields')
                            .hideDelay(3000)
                    );
                }

            }

            cledctl.goToPerson = function (person, event) {
                $state.go('main.clientview', { id: person._id });
            }
        }]);