angular.module('datamanage').factory("testfactory", function ($http, Upload, $q) {
    var factory = {};


    factory.gettestdetails = function (data) {
        var deferred = $q.defer();
        $http({
            method: 'post',
            url: '/gettestdetails',
            data: data
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };



    return factory;

});