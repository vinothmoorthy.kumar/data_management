'use strict';

angular.module('datamanage')
    .controller('testViewCtl', ["$scope", '$mdSidenav', 'testviewresolve', 'clientfactory', '$stateParams', '$mdToast', 'testfactory', '$state', 'empfactory',
        function ($scope, $mdSidenav, testviewresolve, clientfactory, $stateParams, $mdToast, testfactory, $state, empfactory) {
            var tetvew = this;
            tetvew.test = testviewresolve[0];

            if (tetvew.test.result == 1) {
                tetvew.test.result = true;
            } else {
                tetvew.test.result = false;
            }

            tetvew.savetest = function (formdata) {
                var data = formdata;
                // if (data.result) {
                //     data.result = 1
                // } else {
                //     data.result = 0
                // }
                if (data) {
                    if (data.title && data.comment) {
                        empfactory.addtest(data).then(function (response) {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Submited Successfully')
                                    .hideDelay(3000)
                            );
                        })
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Some Fields are missing')
                                .hideDelay(3000)
                        );
                    }
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Fill All The Fields')
                            .hideDelay(3000)
                    );
                }
            }



        }]);