'use strict';

angular.module('datamanage')
    .controller('loginCtrl', ["$scope", '$mdSidenav','AuthenticationService','$location', function ($scope, $mdSidenav,AuthenticationService,$location) {
        $scope.login = function (data) {
            $scope.dataLoading = true;
            AuthenticationService.Login(data.username, data.password, function (response) {
                if (response.currentuser) {
                    AuthenticationService.SetCredentials(response.currentuser, response.token, response.currentusername);
                    $location.path('/client');
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }
            });
        }

    }]);