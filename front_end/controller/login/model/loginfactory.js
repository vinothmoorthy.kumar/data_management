'use strict';
  
angular.module('datamanage')
  
.factory('AuthenticationService',
    ['$http', '$cookieStore', '$rootScope', '$timeout',
    function ( $http, $cookieStore, $rootScope, $timeout) {
        var service = {};
 
        service.Login = function (username, password, callback) {
			var login={
				'username':username,
				'password':password
			}
			$http({
					method:"POST",
					url:"/login",
			 		data:login
				}).success(function datasuccess(data){
                    console.log("111111111111111111",data);
					var response = { currentuser: login.username === data.currentuser,token:data.token,currentusername:data.currentuser};     
					if(!response.currentuser) {
						response.message = data.message;
					}
					callback(response);
				});
 
        };

        
  
        service.SetCredentials = function (username, token,currentuser) {
            var authdata = token;
            $rootScope.globals = {
                currentUser: {
                    username: username,
                    authdata: authdata,
                    currentuser:currentuser
                }
            };
  
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
            $cookieStore.put('globals', $rootScope.globals);
        };
  
        service.ClearCredentials = function () {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic ';
        };

        service.getcurrentuser = function (data) {
            
            var currentuser={
                username:data
            };
            return 		$http({
                method:'post',
                url:'/getcurrentuser',
                data:currentuser
            }).success(function(response){

            }).error();
        };

        
  
        return service;
    }])