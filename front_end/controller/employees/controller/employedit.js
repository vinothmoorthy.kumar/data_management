'use strict';

angular.module('datamanage')
    .controller('empctrl', ["$scope", '$mdSidenav', 'empdetailresolve', 'clientfactory', '$stateParams', '$mdToast', 'empfactory','$state',
        function ($scope, $mdSidenav, empdetailresolve, clientfactory, $stateParams, $mdToast, empfactory,$state) {
            var epctl = this;
            epctl.project = empdetailresolve[0];
            epctl.test = {
                result: false
            }

            epctl.sorttes =function(data){
                if(data =="new"){
                    console.log("888888888");
                    epctl.date=true;
                }else{
                    console.log("999999999");
                   epctl.date=false; 
                }
            }

            epctl.goTotest = function (person, event) {
                $state.go('main.testview', { id: person._id });
            }

            epctl.gettestdata = function () {
                empfactory.gettestlist({ _id: $stateParams.id }).then(function (response) {
                    epctl.testdata = response;
                });
            }

            epctl.savetest = function (formdata) {
                var data = formdata;
                data.company = epctl.project.company;
                data.employee = epctl.project._id;
                if (data) {
                    if (data.title && data.comment) {
                        empfactory.addtest(data).then(function (response) {

                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Submited Successfully')
                                    .hideDelay(3000)
                            );
                            epctl.test = { result: false };
                        })
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Some Fields are missing')
                                .hideDelay(3000)
                        );
                    }
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Fill All The Fields')
                            .hideDelay(3000)
                    );
                }
            }


            epctl.deletetest = function (data) {
                empfactory.deletetest(data).then(function (response) {
                    empfactory.gettestlist({ _id: $stateParams.id }).then(function (response) {
                        epctl.testdata = response;
                    });
                });
            }

            epctl.saveemp = function (formdata) {
                var data = formdata;
                data._id = $stateParams.id;
                if (data) {
                    if (data.name && data.address && data.email && data.phone_number) {
                        clientfactory.addemp(data).then(function (response) {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Submited Successfully')
                                    .hideDelay(3000)
                            );
                        })
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Some Fields are missing')
                                .hideDelay(3000)
                        );
                    }
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Fill All The Fields')
                            .hideDelay(3000)
                    );
                }
            }
        }]);