angular.module('datamanage').factory("empfactory", function ($http, Upload, $q) {
    var factory = {};


    factory.getempdetails = function (data) {
        var deferred = $q.defer();
        $http({
            method: 'post',
            url: '/getempdetails',
            data: data
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };

    factory.addtest = function (data) {
        var deferred = $q.defer();
        $http({
            method: 'post',
            url: '/addtest',
            data: data
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };

    factory.gettestlist = function (data) {
        var deferred = $q.defer();
        $http({
            method: 'post',
            url: '/gettestlist',
            data: data
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };

        factory.deletetest = function (data) {
        var deferred = $q.defer();
        $http({
            method: 'post',
            url: '/deletetest',
            data: data
        }).success(function (response) {
            deferred.resolve(response);
        }).error(function () {
            deferred.reject();
        });
        return deferred.promise;
    };



    




    return factory;

});