'use strict';


angular
  .module('datamanage', [
    'ui.router',
    'ngMaterial',
    'ngCookies',
    'ngFileUpload',
  ]).run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
      $rootScope.globals = $cookieStore.get('globals') || {};
      if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
      }
      $rootScope.$on('$locationChangeStart', function (event, next, current) {
        if ($location.path() == '/login' && $rootScope.globals.currentUser) {
          $location.path('/client');
        }
        if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
          $location.path('/login');
        }


      });
    }])
  .config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {



      $urlRouterProvider.otherwise('/login');

      $stateProvider
        .state('main', {
          templateUrl: './controller/header/view/headereditview.html',
          url: '',
          controller: "AppCtrl",
          controllerAs: 'vm',
          abstract: true
        })
        .state('login', {
          templateUrl: './controller/login/view/loginview.html',
          url: '/login',
          controller: "loginCtrl",
          data: {
            title: 'Dashboard'
          }
        })
        .state('main.empview', {
          templateUrl: './controller/employees/view/employview.html',
          url: '/empdetail/:id',
          controller: "empctrl",
          controllerAs: "EPCTL",
          resolve: {
            empdetailresolve: function (empfactory, $stateParams) {
              return empfactory.getempdetails({ _id: $stateParams.id });
            }
          },
          data: {
            title: 'Employee Details'
          }
        })
        .state('main.client', {
          templateUrl: './controller/client/view/clientlist.html',
          url: '/client',
          controller: "clientCtrl",
          controllerAs: "CLEDCTL",
          resolve: {
            clientlistresolve: function (clientfactory) {
              return clientfactory.getlist();
            }
          },
          data: {
            title: 'Client Screen'
          },
        })
        .state('main.clientview', {
          templateUrl: './controller/client/view/clientview.html',
          url: '/client_details/:id',
          controller: "clientViewCtl",
          controllerAs: "CLVEW",
          resolve: {
            clientviewresolve: function (clientfactory, $stateParams) {
              return clientfactory.getclientdetails({ _id: $stateParams.id });
            }
          },
          data: {
            title: 'Client Screen'
          },

        })
        .state('main.testview', {
          templateUrl: './controller/test/view/test.html',
          url: '/test_details/:id',
          controller: "testViewCtl",
          controllerAs: "TETVEW",
          resolve: {
            testviewresolve: function (testfactory, $stateParams) {
              return testfactory.gettestdetails({ _id: $stateParams.id });
            }
          },
          data: {
            title: 'Test Screen'
          },

        })

    }]).controller('Mainhomectrl', ['$rootScope', '$scope', '', function ($rootScope, $scope) {
      if ($rootScope.globals.currentUser) {
        mainService.getcurrentuser($rootScope.globals.currentUser).then(function (response) {
          $scope.getcurrentuser = response.data[0];
        });
      }
    }])



